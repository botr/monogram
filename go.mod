module pkg.botr.me/monogram

go 1.15

require (
	golang.org/x/text v0.3.5
	gopkg.in/yaml.v2 v2.4.0
)
