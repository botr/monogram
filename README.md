# monogram

Package `monogram` provides convenient access to weights of characters with corresponding tools to generate such mappings.

Package already contains calculated values for **English** and **Russian** languages.  
All letter weights for alphabet normalized so their sum is 1.0.  
It makes them uniform for different character sets.

During generation process all of the letters are converted to UPPERCASE and normalized using `NFC`.


## Usage example

```shell
$ go get pkg.botr.me/monogram@latest
```

```go
package example

import (
    "log"

    "golang.org/x/text/language"
    "pkg.botr.me/monogram"
)

func main() {
    w := monogram.Weights(language.English)
    log.Println(w['E']) // should return ~0.12
}
```

## License

```
Copyright (c) 2021 gudvinr

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
```
