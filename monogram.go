// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Package monogram contains normalized weights for different alphabets.
// Language matching performed using language.Tag from golang.org/x/text/language.
package monogram

import (
	"sort"
	"unicode"

	"golang.org/x/text/language"
)

// RuneWeights describes language character weights for set of runes.
// All frequencies for the language are weighted so sum is always 1.
// Every rune is NFC normalized code point.
type RuneWeights map[rune]float64

var cache = make(map[language.Tag]language.Tag) // cache for language matches

var locales []language.Tag                       // list of languages in weights map
var weights = make(map[language.Tag]RuneWeights) // weights for given language
var vowels = make(map[language.Tag][]rune)       // list of vowel runes for language
var cpw = make(map[language.Tag]float64)         // avg characters per word

// Weights returns RuneWeights for provided Tag. It copies underlying map to avoid accidental modifications to one.
// If provided tag not explicitely defined it will try to find closest match using language.Match with High confidence.
func Weights(tag language.Tag) RuneWeights {
	var rw RuneWeights
	if rw = weights[tag]; rw != nil {
		goto COPY
	}

	if m, ok := cache[tag]; ok {
		rw = weights[m]
		goto COPY
	}

	{
		// NOTE: due to go issue #24211 we can't just use language.Tag from first result of Match function
		_, i, c := language.NewMatcher(locales).Match(tag)
		cache[tag] = locales[i]
		if c >= language.High {
			rw = weights[locales[i]]
		}
	}

COPY:
	if len(rw) > 0 {
		weights := make(RuneWeights)
		for k, v := range rw {
			weights[k] = v
		}
		return weights
	}
	return nil
}

// IsVowel returns true if rune belongs to language and defined as vowel.
// Character transformed to uppercase before checking.
func IsVowel(l language.Tag, r rune) bool {
	r = unicode.ToUpper(r)

	v := vowels[l]
	i := sort.Search(len(v), func(i int) bool { return v[i] >= r })
	return i > -1
}

// CPW returns average number of characters-per-word for provided language
func CPW(tag language.Tag) float64 {
	if n := cpw[tag]; n != 0 {
		return n
	}

	if m, ok := cache[tag]; ok {
		return cpw[m]
	}

	// NOTE: due to go issue #24211 we can't just use language.Tag from first result of Match function
	_, i, c := language.NewMatcher(locales).Match(tag)
	cache[tag] = locales[i]
	if c >= language.High {
		return cpw[locales[i]]
	}

	return 0
}

//go:generate go run internal/gen/char.go ./locales/english.yaml
//go:generate go run internal/gen/char.go ./locales/russian.yaml
