// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Code generated with monogram weights generator. DO NOT EDIT.

package monogram

import "golang.org/x/text/language"

var _en_weights = RuneWeights{
	'A': 0.08551690673195275,
	'B': 0.016047959168228293,
	'C': 0.03164435380900101,
	'D': 0.03871183735737418,
	'E': 0.1209652247516903,
	'F': 0.021815103969122528,
	'G': 0.020863354250923158,
	'H': 0.04955707280570641,
	'I': 0.0732511860723129,
	'J': 0.002197788956104563,
	'K': 0.008086975227142329,
	'L': 0.04206464329306453,
	'M': 0.025263217360184446,
	'N': 0.07172184876283856,
	'O': 0.07467265410810447,
	'P': 0.020661660788966266,
	'Q': 0.0010402453014323196,
	'R': 0.0633271013284023,
	'S': 0.06728203117491646,
	'T': 0.08938126949659495,
	'U': 0.026815809362304373,
	'V': 0.01059346274662571,
	'W': 0.018253618950416498,
	'X': 0.0019135048594134572,
	'Y': 0.017213606152473405,
	'Z': 0.001137563214703838,
}

var _en_vowels = []rune{
	'A',
	'E',
	'I',
	'O',
	'U',
	'Y',
}

func init() {
	loc := language.MustParse("en")
	locales = append(locales, loc)

	weights[loc] = _en_weights
	vowels[loc] = _en_vowels
	cpw[loc] = 4.79
}
