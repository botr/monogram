// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package main

import (
	"flag"
	"fmt"
	"os"
	"path"
	"sort"
	"strings"
	"text/template"

	"golang.org/x/text/unicode/norm"
	"gopkg.in/yaml.v2"
)

const gentmpl = `// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Code generated with monogram weights generator. DO NOT EDIT.

package monogram

import "golang.org/x/text/language"

var _{{ sub .Locale }}_weights = RuneWeights{
{{- range $lit, $weight := .Chars }}
	'{{ $lit }}': {{ $weight.Weight }},
{{- end }}
}

var _{{ sub .Locale }}_vowels = []rune{
{{- range $lit := .Vowels }}
	'{{ $lit }}',
{{- end }}
}

func init() {
	loc := language.MustParse("{{ .Locale }}")
	locales = append(locales, loc)

	weights[loc] = _{{ sub .Locale }}_weights
	vowels[loc] = _{{ sub .Locale }}_vowels
	cpw[loc] = {{ .CPW }}
}
`

type Char struct {
	Weight float64 `yaml:"weight"`
	Vowel  bool    `yaml:"vowel"`
}

type Mapping struct {
	Locale string          `yaml:"locale"`
	CPW    float64         `yaml:"cpw"`
	Chars  map[string]Char `yaml:"chars"`

	Vowels []string `yaml:"-"`
}

func main() {
	flag.Parse()
	if flag.NArg() < 1 {
		panic("no filename provided")
	}

	fname := flag.Arg(0)
	file, err := os.Open(fname)
	errcheck(err)

	var mapping Mapping
	yaml.NewDecoder(file).Decode(&mapping)
	file.Close()

	// Input character frequencies might not be normalized.
	// Calculate total sum and then divide so weight of the alphabet would be 1.0 for every language.
	var sum float64
	for _, c := range mapping.Chars {
		sum += c.Weight
	}

	// props are copied to new map since we also perform NFC normalization
	chars := make(map[string]Char, len(mapping.Chars))
	for r, char := range mapping.Chars {
		r = norm.NFC.String(r)
		char.Weight = char.Weight / sum
		chars[r] = char

		if char.Vowel {
			mapping.Vowels = append(mapping.Vowels, r)
		}
	}
	mapping.Chars = chars

	sort.Slice(mapping.Vowels, func(i, j int) bool {
		return mapping.Vowels[i] < mapping.Vowels[j]
	})

	// prepare generated file
	fname = strings.TrimSuffix(path.Base(fname), path.Ext(fname))
	fname = fmt.Sprintf("chars_%s_gen.go", fname)

	file, err = os.OpenFile(fname, os.O_CREATE|os.O_WRONLY, 0644)
	errcheck(err)
	defer file.Close()

	// replace dashes as they can't be used in variable names
	sub := func(s string) string { return strings.ReplaceAll(s, "-", "_") }

	tmpl := template.Must(template.
		New("map").
		Funcs(template.FuncMap{"sub": sub}).
		Parse(gentmpl),
	)

	err = tmpl.Execute(file, &mapping)
	errcheck(err)
}

func errcheck(err error) {
	if err != nil {
		panic(err)
	}
}
