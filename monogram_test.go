// Copyright (c) 2021 gudvinr
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package monogram

import (
	"testing"

	"golang.org/x/text/language"
)

func TestExistingLanguage(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		weights := Weights(language.MustParse("en"))
		if weights == nil {
			t.Fatal()
		}
	})

	t.Run("Predefined", func(t *testing.T) {
		weights := Weights(language.Russian)
		if weights == nil {
			t.Fatal()
		}
	})
}

func TestBaseLanguage(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		weights := Weights(language.MustParse("ru-UA"))
		if weights == nil {
			t.Fatal()
		}
	})

	t.Run("Predefined", func(t *testing.T) {
		weights := Weights(language.BritishEnglish)
		if weights == nil {
			t.Fatal()
		}
	})
}

func TestInexistingLanguage(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		weights := Weights(language.MustParse("ca"))
		if weights != nil {
			t.Fatal()
		}
	})

	t.Run("Predefined", func(t *testing.T) {
		weights := Weights(language.Spanish)
		if weights != nil {
			t.Fatal()
		}
	})

	t.Run("Undefined", func(t *testing.T) {
		weights := Weights(language.Make("und-419"))
		if weights != nil {
			t.Fatal()
		}
	})

	t.Run("Undefined", func(t *testing.T) {
		weights := Weights(language.Und)
		if weights != nil {
			t.Fatal()
		}
	})
}
